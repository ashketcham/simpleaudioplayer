# SimpleAudioPlayer

A simple Audio Player using HTML, JS, JQuery.
You can use mouse on bars to skip to certain position for audio.
Mute button also can be used to mute the audio.

## Use below link to access the audio player.
[SimpleAudioPlayer](https://practical-brattain-2e4fec.netlify.app/)
