const maxSize = 100;
const barColor = "plum"

function getRngInteger(min=50, max=100) {
    return Math.floor(Math.random() * (max - min + 1) )  + min;
}

function generateHeights() {
    let heights = []
    for (let i=0; i<=maxSize; i++) {
        heights.push(getRngInteger());
    }
    return  heights;
}

function generateDirection() {
    let direction = [];
    for (let i =0 ; i<=maxSize; i++) {
        if (Math.random() <= 0.5) 
            direction.push(1);
        else
            direction.push(-1);
    }
    return direction;
}

function playAudio() {
    const ap = document.getElementById("audio-player");
    ap.play();
    $("#playButton").hide();
    $("#pauseButton").show();
}

function pauseAudio() {
    const ap = document.getElementById("audio-player");
    ap.pause();
    $("#playButton").show();
    $("#pauseButton").hide();
}

function mute() {
    const ap = document.getElementById("audio-player");
    ap.muted = true;
    $("#mute").hide();
    $("#unmute").show();
}

function unmute() {
    const ap = document.getElementById("audio-player");
    ap.muted = false;
    $("#unmute").hide();
    $("#mute").show();
}

function getMousePosition(canvas, event) {
    let rect = canvas.getBoundingClientRect();
    let x = event.clientX - rect.left;
    let y = event.clientY - rect.top;
    console.log("Posigion" + x + " : " + y);
}

function clearBars() {
    let ctx = canvas.getContext('2d');
    ctx.fillStyle= "gray"
    let x = 0;
    let y = canvasHeight / 2;
    for (let i=0; i<maxSize; i++) {
        ctx.fillRect(x, y, width, direction[i] * heightsTop[i]);
        ctx.fillRect(x, y, width, -1 * direction[i] * heightsBottom[i]);
        x = x + inc;
    }
}

function drawFirstTag(canvas) {
    let ctx = canvas.getContext('2d');
    ctx.fillStyle = "green";
    ctx.fillRect(100, 70, 115, 22);
    ctx.fillStyle = "white"
    ctx.textBaseline = "middle"
    ctx.textAligh = "center";
    ctx.font = "15px Comic Sans MS";
    ctx.fillText("Introduction", 110, 83);
    // create arc
    ctx.beginPath();
    ctx.fillStyle = "green"
    ctx.arc(150, 200, 8, 0, 2 * Math.PI);
    ctx.fill();
    // draw line between arc and text
    ctx.fillRect(148, 92, 4, 100);
}

function drawSecondTag(canvas) {
    let ctx = canvas.getContext('2d');
    ctx.fillStyle = "#2bedd6";
    ctx.fillRect(300, 30, 80, 22);
    ctx.fillStyle = "white"
    ctx.textBaseline = "middle"
    ctx.textAligh = "center";
    ctx.font = "15px Comic Sans MS";
    ctx.fillText("one_six", 310, 43);
    // create arc
    ctx.beginPath();
    ctx.fillStyle = "#2bedd6"
    ctx.arc(340, 200, 8, 0, 2 * Math.PI);
    ctx.fill();
    // draw line between arc and text
    ctx.fillRect(338, 52, 4, 150);
}

function drawThirdTag(canvas) {
    let ctx = canvas.getContext('2d');
    ctx.fillStyle = "blue";
    ctx.fillRect(600, 70, 70, 22);
    ctx.fillStyle = "white"
    ctx.textBaseline = "middle"
    ctx.textAligh = "center";
    ctx.font = "15px Comic Sans MS";
    ctx.fillText("Profile", 610, 83);
    // create arc
    ctx.beginPath();
    ctx.fillStyle = "blue"
    ctx.arc(640, 200, 8, 0, 2 * Math.PI);
    ctx.fill();
    // draw line between arc and text
    ctx.fillRect(638, 92, 4, 100);
}

function drawForthTag(canvas) {
    let ctx = canvas.getContext('2d');
    ctx.fillStyle = "#7e5996";
    ctx.fillRect(600, 40, 190, 22);
    ctx.fillStyle = "white"
    ctx.textBaseline = "middle"
    ctx.textAligh = "center";
    ctx.font = "15px Comic Sans MS";
    ctx.fillText("Report Building-Empathy", 610, 53);
    // create arc
    ctx.beginPath();
    ctx.fillStyle = "#7e5996"
    ctx.arc(740, 200, 8, 0, 2 * Math.PI);
    ctx.fill();
    // draw line between arc and text
    ctx.fillRect(738, 62, 4, 140);
}

function drawFifthTag(canvas) {
    let ctx = canvas.getContext('2d');
    ctx.fillStyle = "green";
    ctx.fillRect(650, 10, 190, 22);
    ctx.fillStyle = "white"
    ctx.textBaseline = "middle"
    ctx.textAligh = "center";
    ctx.font = "15px Comic Sans MS";
    ctx.fillText("Report Building-Energy", 660, 23);
    // create arc
    ctx.beginPath();
    ctx.fillStyle = "green"
    ctx.arc(790, 200, 8, 0, 2 * Math.PI);
    ctx.fill();
    // draw line between arc and text
    ctx.fillRect(788, 32, 4, 170);
}

function drawTags(canvas) {
    drawFirstTag(canvas);
    drawSecondTag(canvas);
    drawThirdTag(canvas);
    drawForthTag(canvas);
    drawFifthTag(canvas);
}
let heightsTop = generateHeights();
let heightsBottom = generateHeights();
let direction = generateDirection();
let width = 5;
let inc = 10;

const canvas = document.getElementById("audio-canvas");
let canvasWidth = canvas.width;
let canvasHeight = canvas.height;
if (canvas.getContext) {
    let ctx = canvas.getContext('2d');
    ctx.fillStyle= "gray"
    let x = 0;
    let y = canvasHeight / 2;
    for (let i=0; i<maxSize; i++) {
        ctx.fillRect(x, y, width, direction[i] * heightsTop[i]);
        ctx.fillRect(x, y, width, -1 * direction[i] * heightsBottom[i]);
        x = x + inc;
    }

    //TODO- draw tags on bars
    drawTags(canvas);
}

document.addEventListener("input", function(event) {
    const audioSrc = document.getElementById("audio-player");
    const uploadFile = document.getElementById("upload-file");
    audioSrc.src = uploadFile.value;
});

document.addEventListener('mousedown', function(event) {
    let clickedTarget = event.target;
    let clickedPosition = 0;
    if (clickedTarget == canvas) {
        getMousePosition(canvas, event);
        let ctx = canvas.getContext('2d');
        ctx.fillStyle = barColor;
        let x = 0;
        let rect = canvas.getBoundingClientRect();
        let paintTill = (event.clientX - rect.left)/10 * 10;
        let y = canvasHeight / 2;
        for (let i=0; i<maxSize; i++) {
            if ( x < paintTill) {
                ctx.fillRect(x, y, width, direction[i] * heightsTop[i]);
                ctx.fillRect(x, y, width, -1 * direction[i] * heightsBottom[i]);
                clickedPosition = i;
            } else {
                ctx.fillStyle = "gray";
                ctx.fillRect(x, y, width, direction[i] * heightsTop[i]);
                ctx.fillRect(x, y, width, -1 * direction[i] * heightsBottom[i]);
            }
            x = x + inc;
        }
        let ap = document.getElementById("audio-player");
        console.log("audio details: ", ap.currentTime, ap.duration);
        console.log("canvas click details", clickedPosition, maxSize);
        console.log("percentage");
        console.log(ap.currentTime * 100 / ap.duration, clickedPosition * 100 / maxSize);
        ap.currentTime = ((clickedPosition) / maxSize) * ap.duration;
        console.log("AFTER CHANGE: ", ap.currentTime);
    }
});

let ap = document.getElementById("audio-player");
ap.addEventListener("timeupdate", function(event) {
    let ctx = canvas.getContext('2d');
    ctx.fillStyle = barColor;
    let i = parseInt((this.currentTime / this.duration) * maxSize);
    console.log("itopaing: ", i);
    let x = 0;
    let y = canvasHeight / 2;
    ctx.fillRect(i * inc, y, width, direction[i] * heightsTop[i]);
    ctx.fillRect(i * inc, y, width, -1 * direction[i] * heightsBottom[i]);
    drawTags(canvas);
});

ap.addEventListener("ended", function() {
    pauseAudio();
    clearBars();
    drawTags(canvas);
});

// ap.addEventListener("seeked", function(event) {
//     let ctx = canvas.getContext('2d');
//     ctx.fillStyle = "barColor";
//     let x = 0;
//     let y = canvasHeight / 2;
//     // let i = 0;
//     let iTobarColor = parseInt((this.currentTime / this.duration) * maxSize);
//     for (let i=0; i<maxSize; i++) {
//         if ( i <= iToRed) {
//             ctx.fillRect(x, y, width, direction[i] * heightsTop[i]);
//             ctx.fillRect(x, y, width, -1 * direction[i] * heightsBottom[i]);
//             clickedPosition = i;
//         } else {
//             ctx.fillStyle = "gray";
//             ctx.fillRect(x, y, width, direction[i] * heightsTop[i]);
//             ctx.fillRect(x, y, width, -1 * direction[i] * heightsBottom[i]);
//         }
//         x = x + inc;
//     }
// });
